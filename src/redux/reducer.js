import { createSlice } from "@reduxjs/toolkit"

const counterSlice = createSlice({
    name:'counter',
    initialState:{
        count:0,
    },
    reducers:{
        increment:(state,action)=>{
            ++state.count;
        },
        decrement:(state)=>{
          --state.count;
        },
        incrementByAmount:(state,action)=>{
            console.log(action);
            state.count+=action.payload
        }
    }
})

export const {increment,decrement,incrementByAmount} = counterSlice.actions

export default counterSlice.reducer