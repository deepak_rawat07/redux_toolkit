import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { decrement, increment, incrementByAmount } from './redux/reducer';

const App = () => {
  const count = useSelector((state) => {
    console.log(state);
    return state.counter.count});
const dispatch = useDispatch();
const [countBy, setcountBy] = useState(0)
  return (
    <div>
      Count is :{count}
      <button onClick={() => dispatch(increment())}>Increment</button>
      <button onClick={() => dispatch(decrement())}>Decrement</button>
      <input
        type="number"
        onChange={(event) => setcountBy(Number(event.target.value))}
      />
      <button onClick={() => dispatch(incrementByAmount(countBy))}>
        Increment By {countBy}
      </button>
    </div>
  );
}

export default App
